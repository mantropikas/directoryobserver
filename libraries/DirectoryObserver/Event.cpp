//
// Created by Mantas Antropikas on 09/05/2018.
//

#include "Event.h"

std::ostream& operator<<(std::ostream& out, const EventType& eventType)
{
    switch (eventType)
    {
        case EventType::FILE_CREATED:
            out << "File created";
            break;
        case EventType::FILE_MODIFIED:
            out << "File modified";
            break;
        case EventType::FILE_DELETED:
            out << "File deleted";
            break;
        case EventType::ERROR:
            out << "ERROR";
            break;
    }

    return out;
}

Event::Event(const file::File& file, EventType eventType)
        : m_file(file)
        , m_eventType(eventType)
{}

const file::File& Event::getFile() const
{
    return m_file;
}

EventType Event::getEventType() const
{
    return m_eventType;
}
