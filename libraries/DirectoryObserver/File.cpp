//
// Created by Mantas Antropikas on 10/05/2018.
//

#include "File.h"
#include "FileUtils.h"
#include "Utils.h"

#include <iostream>

std::ostream& file::operator<<(std::ostream& out, const file::FileType& fileType)
{
    switch (fileType)
    {
        case file::FileType::FILE:
            out << "Regular file";
            break;
        case file::FileType::DIRECTORY:
            out << "Directory";
            break;
        case FileType::UNKNOWN:
            out << "Unknown file";
            break;
    }

    return out;
}

file::File::File(const fs::path& path)
        : m_path(path)
        , m_exists(utils::file::fileExists(m_path))
        , m_type(utils::file::getFileType(m_path))
        , m_lastWriteTime(utils::file::getLastWriteTime(m_path))
{}

const fs::path& file::File::getPath() const
{
    return m_path;
}

bool file::File::exists() const
{
    return m_exists;
}

file::FileType file::File::getType() const
{
    return m_type;
}

time_t file::File::getLastWriteTime() const
{
    return m_lastWriteTime;
}
