//
// Created by Mantas Antropikas on 10/05/2018.
//

#pragma once

#include <string>
#include <ctime>

namespace utils {
    std::string getCurrentUniversalTime();

    std::string universalTimeToString(std::time_t time);

    void pauseThread(long millis);
}
