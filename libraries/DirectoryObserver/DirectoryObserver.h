//
// Created by Mantas Antropikas on 09/05/2018.
//

#pragma once

#include "File.h"
#include "Event.h"

#include <vector>
#include <functional>
#include <thread>
#include <unordered_map>

struct FilesystemPathHasher {
    std::size_t operator()(const fs::path& k) const
    {
        return std::hash<std::string>()(k.string());
    }
};

class DirectoryObserver
{
public:
    DirectoryObserver(const fs::path& path,
                        std::function<void(const Event& event)> callback);

    DirectoryObserver(const std::vector<fs::path>& filePaths,
                        std::function<void(const Event& event)> callback);

    void start();

    void stop();

    /// This method will add the file with given path among observable files if that file exists and its type is known,
    /// i.e. directory or regular file
    ///
    /// @param path The file or directory path. If given path is a directory, all its files will be observed as well
    void startObserving(const fs::path& path);

    /// This method will remove the file with given path from observable files list. If file is a directory all directory
    /// files will be removed from observable files list as well
    ///
    /// @param path The file or directory path. If given path is a directory, all its files will no longer be observed
    void stopObserving(const fs::path& path);

    int getObservableFileCount();

private:
    /// If start was invoked this will start observing observable file' status change
    /// Invocation of stop will pause observation the observable file' status change
    void observe();

    void checkFileStatus(const file::File& file,
                         std::unordered_map<fs::path, file::File, FilesystemPathHasher>& observableFiles,
                         std::unordered_map<fs::path, file::File, FilesystemPathHasher>& createdFiles);

    void updateObservableFilesList(const file::File& directory,
                                   std::unordered_map<fs::path, file::File, FilesystemPathHasher>& observableFiles);

    void checkIfFileIsModified(const file::File& file,
                               const file::File& originalCacheEntry);

private:
    const std::function<void(const Event& event)> m_callback;

    std::unordered_map<fs::path, file::File, FilesystemPathHasher> m_observedFiles;
    std::mutex m_observableFilesLock;

    std::thread m_observerThread;
    bool m_observing = false;
    std::mutex m_observerThreadLock;
};
