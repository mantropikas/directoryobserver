//
// Created by Mantas Antropikas on 09/05/2018.
//

#include "DirectoryObserver.h"
#include "Utils.h"
#include "FileUtils.h"

#include "boost/filesystem.hpp"
#include "boost/algorithm/string/predicate.hpp"

#include <sstream>
#include <AppleTextureEncoder.h>

namespace fs = boost::filesystem;

DirectoryObserver::DirectoryObserver(const fs::path& path,
                                     std::function<void(const Event& event)> callback)
        : m_callback(std::move(callback))
{
    startObserving(path);
}

DirectoryObserver::DirectoryObserver(const std::vector<fs::path>& filePaths,
                                     std::function<void(const Event& event)> callback)
        : m_callback(std::move(callback))
{
    for (const auto& path : filePaths)
    {
        startObserving(path);
    }
}

void DirectoryObserver::start()
{
    std::lock_guard<std::mutex> lock(m_observerThreadLock);
    if (!m_observing)
    {
        m_observing = true;
        m_observerThread = std::thread(&DirectoryObserver::observe, this);
    }
}

void DirectoryObserver::stop()
{
    std::lock_guard<std::mutex> lock(m_observerThreadLock);
    if (m_observing)
    {
        m_observing = false;
        m_observerThread.join();
    }
}

void DirectoryObserver::startObserving(const fs::path& path)
{
    file::File file(path);
    if (file.getType() == file::FileType::UNKNOWN)
    {
        Event event(file, EventType::ERROR);
        m_callback(event);
        return;
    }

    std::unique_lock<std::mutex> lock(m_observableFilesLock);
    fs::path canonicalPath = fs::canonical(path, "");
    if (m_observedFiles.find(canonicalPath) == m_observedFiles.end())
    {
        m_observedFiles.emplace(canonicalPath, file);
        lock.unlock();
    }
    else
    {
        lock.unlock();
        return;
    }

    if (file.getType() == file::FileType::DIRECTORY)
    {
        fs::directory_iterator end; // creates the "end" iterator
        for (fs::directory_iterator it(file.getPath()); it != end; ++it)
        {
            startObserving(it->path());
        }
    }
}

void DirectoryObserver::stopObserving(const fs::path& path)
{
    fs::path pathToDiscard = fs::exists(path) ? fs::canonical(path, "") : path;

    std::lock_guard<std::mutex> lock(m_observableFilesLock);
    for (auto it = m_observedFiles.begin(); it != m_observedFiles.end();)
    {
        fs::path observablePath = fs::exists(observablePath) ? fs::canonical(it->first, "") : it->first;
        bool remove = false;

        while (!observablePath.empty())
        {
            if (observablePath == pathToDiscard)
            {
                remove = true;
                break;
            }
            else
            {
                observablePath = observablePath.parent_path();
            }
        }

        if (remove)
        {
            it = m_observedFiles.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

int DirectoryObserver::getObservableFileCount()
{
    std::lock_guard<std::mutex> lock(m_observableFilesLock);
    return static_cast<int>(m_observedFiles.size());
}

void DirectoryObserver::observe()
{
    while (m_observing)
    {
        std::unordered_map<fs::path, class file::File, FilesystemPathHasher> observedFiles;
        // At first we need to gather all files that were created and only then start to monitor them
        std::unordered_map<fs::path, class file::File, FilesystemPathHasher> createdFiles;

        std::unique_lock<std::mutex> lock(m_observableFilesLock);
        observedFiles.insert(m_observedFiles.begin(), m_observedFiles.end());
        lock.unlock();

        while (!observedFiles.empty())
        {
            auto entry = observedFiles.begin();
            auto observablePath = entry->first;
            auto observableFile = entry->second;

            // Create new instance of file (this reads all updates of file: size, presence)
            file::File f(observablePath);
            checkFileStatus(f, observedFiles, createdFiles);

            observedFiles.erase(entry);
        }

        lock.lock();
        m_observedFiles.insert(createdFiles.begin(), createdFiles.end());
        lock.unlock();

        utils::pauseThread(25);
    }
}

void DirectoryObserver::checkFileStatus(const file::File& file,
                                        std::unordered_map<fs::path, file::File, FilesystemPathHasher>& observableFiles,
                                        std::unordered_map<fs::path, file::File, FilesystemPathHasher>& createdFiles)
{
    // Check whether file was removed
    if (!file.exists())
    {
        Event event(file, EventType::FILE_DELETED);
        m_callback(event);
        // File was removed, stopping to monitor it!
        stopObserving(file.getPath());
        return;
    }

    // Execute file status check for every file in directory
    if (file.getType() == file::FileType::DIRECTORY)
    {
        updateObservableFilesList(file, observableFiles);
    }

    std::unique_lock<std::mutex> lock(m_observableFilesLock);
    auto it = m_observedFiles.find(file.getPath());
    bool found = it != m_observedFiles.end();
    lock.unlock();

    if (found)
    {
        file::File fileEntryFromCache = it->second;
        checkIfFileIsModified(file, fileEntryFromCache);
    }
    else
    {
        // Entry not found, i.e. file was recently created
        Event event(file, EventType::FILE_CREATED);
        m_callback(event);

        // File was created in the directory which we monitor - this file should be monitored as well
        createdFiles.emplace(file.getPath(), file);
    }
}

void DirectoryObserver::checkIfFileIsModified(const file::File& file,
                                              const file::File& originalCacheEntry)
{
    // Checking whether file was modified
    if (file.getLastWriteTime() != originalCacheEntry.getLastWriteTime())
    {
        // Update cache entry for this file
        std::unique_lock<std::mutex> lock(m_observableFilesLock);
        m_observedFiles.insert_or_assign(file.getPath(), file);
        lock.unlock();

        if (file.getType() != originalCacheEntry.getType())
        {
            Event eventCreatedFile(file, EventType::FILE_CREATED);
            m_callback(eventCreatedFile);

            Event eventDeletedFile(originalCacheEntry, EventType::FILE_DELETED);
            m_callback(eventDeletedFile);

            lock.lock();
            // Notify that parent directory has been modified if it's being monitored
            auto parentDirectory = m_observedFiles.find(fs::canonical(originalCacheEntry.getPath()).parent_path());
            if (parentDirectory != m_observedFiles.end())
            {
                Event directoryModification(parentDirectory->second, EventType::FILE_MODIFIED);
                m_callback(directoryModification);
            }

            lock.unlock();
        }
        else
        {
            Event event(file, EventType::FILE_MODIFIED);
            m_callback(event);
        }
    }
}

void DirectoryObserver::updateObservableFilesList(const file::File& directory,
                                                  std::unordered_map<fs::path, file::File, FilesystemPathHasher>& observableFiles)
{
    // Create the "end of directory files" point
    fs::directory_iterator end;
    for (fs::directory_iterator it(directory.getPath()); it != end; ++it)
    {
        const file::File file(it->path().string());
        observableFiles.insert_or_assign(file.getPath(), file);
    }
}
