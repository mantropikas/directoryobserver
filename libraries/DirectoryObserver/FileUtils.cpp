//
// Created by Mantas Antropikas on 16/05/2018.
//

#include "FileUtils.h"
#include "Utils.h"

#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;

bool utils::file::fileExists(const fs::path& filePath)
{
    return fs::exists(filePath);
}

file::FileType utils::file::getFileType(const fs::path& filePath)
{
    if (fs::exists(filePath))
    {
        if (fs::is_directory(filePath))
        {
            return ::file::FileType::DIRECTORY;
        }
        else if (fs::is_regular_file(filePath))
        {
            return ::file::FileType::FILE;
        }
    }

    return ::file::FileType::UNKNOWN;
}

time_t utils::file::getLastWriteTime(const fs::path& filePath)
{
    return getFileType(filePath) != ::file::FileType::UNKNOWN ? fs::last_write_time(filePath)
                                                              : 0;
}
