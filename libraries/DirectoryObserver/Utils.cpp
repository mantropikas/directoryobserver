//
// Created by Mantas Antropikas on 10/05/2018.
//

#include "Utils.h"

#include "boost/date_time/posix_time/posix_time.hpp"

#include <thread>

namespace pt = boost::posix_time;

std::string utils::getCurrentUniversalTime()
{
    return pt::to_simple_string(pt::second_clock::universal_time());
}

std::string utils::universalTimeToString(std::time_t time)
{
    return pt::to_simple_string(pt::from_time_t(time));
}

void utils::pauseThread(long millis)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(millis));
}
