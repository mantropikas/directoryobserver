//
// Created by Mantas Antropikas on 16/05/2018.
//

#pragma once

#include "File.h"

namespace utils {
    namespace file {
        bool fileExists(const fs::path& filePath);

        ::file::FileType getFileType(const fs::path& filePath);

        time_t getLastWriteTime(const fs::path& filePath);
    }
}
