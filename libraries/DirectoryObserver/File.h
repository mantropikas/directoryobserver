//
// Created by Mantas Antropikas on 10/05/2018.
//

#pragma once

#include "boost/filesystem.hpp"

#include <string>
#include <ctime>

namespace fs = boost::filesystem;

namespace file {
    enum class FileType
    {
        UNKNOWN,
        FILE,
        DIRECTORY
    };

    std::ostream& operator<<(std::ostream& out, const file::FileType& fileType);

    class File
    {
    public:
        explicit File(const fs::path& path);

        const fs::path& getPath() const;

        bool exists() const;

        FileType getType() const;

        time_t getLastWriteTime() const;

    private:
        fs::path m_path;
        bool m_exists;
        FileType m_type;
        time_t m_lastWriteTime;
    };
}
