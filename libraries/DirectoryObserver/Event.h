//
// Created by Mantas Antropikas on 09/05/2018.
//

#pragma once

#include "File.h"

#include <string>
#include <iostream>

enum class EventType
{
    ERROR,
    FILE_CREATED,
    FILE_MODIFIED,
    FILE_DELETED
};

std::ostream& operator<<(std::ostream& out, const EventType& eventType);

class Event
{
public:
    Event(const file::File& file, EventType eventType);

    const file::File& getFile() const;

    EventType getEventType() const;

private:
    file::File m_file;
    EventType m_eventType;
};
