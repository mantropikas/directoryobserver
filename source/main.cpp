#include <iostream>
#include "DirectoryObserver.h"
#include "Utils.h"

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " FILES...\n";
        return 1;
    }

    auto callback = [](const Event& event) -> void
    {
        std::cout << utils::getCurrentUniversalTime() << ": " << event.getEventType() << " - file path "
                  << event.getFile().getPath() << "\n";
    };

    std::vector<fs::path> paths;
    paths.reserve(argc - 1);
    for (int i = 1; i < argc; i++)
    {
        paths.emplace_back(argv[i]);
    }

    std::cout << "Starting DirectoryObserver\n";
    std::cout << "\tEnter 's' or 'S' to start/re-start observing files\n";
    std::cout << "\tEnter 'p' or 'P' to pause\n";
    std::cout << "\tEnter 'q' or 'Q' to exit\n";
    DirectoryObserver observer(paths, callback);

    int keyChar = 0;

    // This will allow to extend the functionality of DirectoryObserver executable, e.g.:
    // - add new files to be observed
    // - remove file from observable files list
    // - get list of observable files
    std::string command;

    while (command != "q" && command != "Q")
    {
        if (command == "s" || command == "S")
        {
            observer.start();
        }
        else if (command == "p" || command == "P")
        {
            observer.stop();
        }

        std::cin >> command;
    }

    observer.stop();

    return 0;
}