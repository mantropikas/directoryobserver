//
// Created by Mantas Antropikas on 16/05/2018.
//

#pragma once

#include <string>
#include <File.h>

const std::string PATH = "./TEST";
const std::string CONTENT = "## INITIAL FILE CONTENT ##";
const auto CONTENT_SIZE = CONTENT.size();

namespace utils {
    namespace test {
        bool isDirectory(const fs::path& name);

        bool isFile(const fs::path& fileName, const fs::path& directoryName = "");

        bool createDirectory(const fs::path& name);

        bool createFile(const fs::path& fileName, const fs::path& directoryName = "");

        bool deleteFile(const fs::path& fileName, const fs::path& directoryName = "");

        bool forceDeleteDirectory(const fs::path& directoryName);

        bool
        writeDataToFile(const std::string& data,
                        const fs::path& fileName,
                        const fs::path& directoryName = "",
                        bool append = false);

        // getFile("", "") will return file::File which points to the "./TEST/"
        // getFile("", "") will return file::File which points to the "./TEST/"
        file::File getFile(const fs::path& fileName, const fs::path& directoryName = "");
    }
}
