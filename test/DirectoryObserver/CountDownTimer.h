//
// Created by Mantas Antropikas on 23/05/2018.
//

#pragma once

class CountDownTimer
{
public:
    void start();

    void reStart();

private:
    void countDown();

private:
    bool m_countingDown = false;
    long m_ttl = 1000; // 1s
};
