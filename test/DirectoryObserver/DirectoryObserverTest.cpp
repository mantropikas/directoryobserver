//
// Created by Mantas Antropikas on 16/05/2018.
//

#include "DirectoryObserver.h"
#include "Utils.h"

#include "TestUtils.h"
#include "CountDownTimer.h"

#include "gtest/gtest.h"
#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;

namespace {
    std::mutex m_lock;
    std::vector<Event> m_events;
    CountDownTimer m_timer;

    bool createFile(const fs::path& filePath)
    {
        fs::fstream writer;
        writer.open(filePath, std::ios_base::out);
        writer << CONTENT;
        writer.close();
        return is_regular_file(filePath) && file_size(filePath) != 0;
    }

    void handleEvent(const Event& event)
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_timer.reStart();
        switch (event.getEventType())
        {
            case EventType::ERROR:
                break;
            case EventType::FILE_CREATED:
            case EventType::FILE_MODIFIED:
                std::cout << event.getEventType() << ": path '" << event.getFile().getPath()
                          << "', modification time: "
                          << utils::universalTimeToString(event.getFile().getLastWriteTime())
                          << "\n";
                break;
            case EventType::FILE_DELETED:
                std::cout << event.getEventType() << ": path '" << event.getFile().getPath() << "'\n";
                break;
        }

        m_events.push_back(event);
    }

    void cleanEvents()
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if (!m_events.empty())
        {
            m_events.erase(m_events.begin(), m_events.end());
        }
    }

    unsigned long getEventCount()
    {
        std::lock_guard<std::mutex> lock(m_lock);
        return m_events.size();
    }

    bool eventExistsForPath(const fs::path& path, const EventType& eventType, int count = 1)
    {
        auto fullPath = fs::exists(path) ? fs::canonical(path, "") : path;
        auto matchesCount = std::count_if(m_events.begin(),
                                          m_events.end(),
                                          [&fullPath, &eventType](const Event& event)
                                          {
                                              return event.getFile().getPath() == fullPath &&
                                                     event.getEventType() == eventType;
                                          });

        return count == matchesCount;
    }
}

class DirectoryObserverTest : public ::testing::Test
{
protected:
    void SetUp() override;

    void startObserving();

    void stopObserving();

    void TearDown() override;

    DirectoryObserver* m_observer = nullptr;
};

void DirectoryObserverTest::SetUp()
{
    Test::SetUp();

    const fs::path root(PATH);

    fs::path path(root);
    path.append("1");

    fs::create_directories(path);
    bool createdDir = fs::is_directory(path);

    bool createdDir0 = fs::create_directories(root / "D0");
    bool createdDir1 = fs::create_directories(root / "D1");
    bool createdDir2 = fs::create_directories(root / "D2");

    bool createdFile0 = createFile(root / "F0");
    bool createdFile1 = createFile(root / "F1");
    bool createdFile2 = createFile(root / "F2");

    ASSERT_TRUE(createdDir);

    ASSERT_TRUE(createdDir0);
    ASSERT_TRUE(createdDir1);
    ASSERT_TRUE(createdDir2);

    ASSERT_TRUE(createdFile0);
    ASSERT_TRUE(createdFile1);
    ASSERT_TRUE(createdFile2);

    m_observer = new DirectoryObserver(root, handleEvent);

    m_timer.start();
    startObserving();
}

void DirectoryObserverTest::startObserving()
{
    if (m_observer != nullptr)
    {
        m_observer->start();
    }
}

TEST_F(DirectoryObserverTest, NoAction)
{
    ASSERT_EQ(0, getEventCount());
}

// mkdir dir1
TEST_F(DirectoryObserverTest, CreateDirectory)
{
    const auto dirName = "dir1";
    utils::test::createDirectory(dirName);

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirName, EventType::FILE_CREATED));
}

// mkdir dir1 dir2
TEST_F(DirectoryObserverTest, CreateTwoDirectories)
{
    const auto dirName = "dir1";
    const auto dirName2 = "dir2";
    utils::test::createDirectory(dirName);
    utils::test::createDirectory(dirName2);

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirName, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirName2, EventType::FILE_CREATED));
}

// mkdir -p dir1/dir2
TEST_F(DirectoryObserverTest, CreateTwoDirectoriesRecursively)
{
    std::stringstream ss;
    ss << "dir1";
    auto dirPath1 = ss.str();

    ss << fs::path::preferred_separator << "dir2";
    auto dirPath2 = ss.str();

    utils::test::createDirectory(dirPath1);
    utils::test::createDirectory(dirPath2);

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath1, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath2, EventType::FILE_CREATED));
}

// mkdir newD0
// sleep 1
// mkdir newD1
TEST_F(DirectoryObserverTest, CreateTwoDirectoriesWithDelay)
{
    utils::test::createDirectory("newD0");
    utils::pauseThread(1000);
    utils::test::createDirectory("newD1");

    m_timer.start();
    ASSERT_EQ(4, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "newD0", EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "newD1", EventType::FILE_CREATED));
}

// mkdir -p dir1/dir2
// mkdir -p dir3/dir4
TEST_F(DirectoryObserverTest, CreateTwoDirectoriesRecursivelyTwice)
{
    std::stringstream ss1;
    ss1 << "dir1" << fs::path::preferred_separator << "dir2";
    std::stringstream ss2;
    ss2 << "dir1" << fs::path::preferred_separator << "dir2";
    auto dirPath1 = ss1.str();
    auto dirPath2 = ss2.str();

    utils::test::createDirectory(dirPath1);
    utils::test::createDirectory(dirPath2);

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath1, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath2, EventType::FILE_CREATED));
}

// mkdir -p dir1/dir2
// sleep 1
// mkdir -p dir3/dir4
TEST_F(DirectoryObserverTest, CreateTwoDirectoriesRecursivelyTwiceWithDelay)
{
    std::stringstream ss1;
    std::stringstream ss2;

    ss1 << "dir1";
    ss2 << "dir3";
    auto dirPath1 = ss1.str();
    auto dirPath3 = ss2.str();

    ss1 << fs::path::preferred_separator << "dir2";
    ss2 << fs::path::preferred_separator << "dir4";
    auto dirPath1_2 = ss1.str();
    auto dirPath3_4 = ss2.str();

    utils::test::createDirectory(dirPath1_2);
    utils::pauseThread(1000);
    utils::test::createDirectory(dirPath3_4);

    m_timer.start();
    ASSERT_EQ(6, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath1, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath1_2, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath3, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / dirPath3_4, EventType::FILE_CREATED));
}

// touch file
TEST_F(DirectoryObserverTest, CreateFile)
{
    const auto fileName = "file";
    utils::test::createFile(fileName);

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / fileName, EventType::FILE_CREATED));
}

// touch file file2
TEST_F(DirectoryObserverTest, CreateTwoFiles)
{
    const auto fileName = "file";
    const auto fileName2 = "file2";
    utils::test::createFile(fileName);
    utils::test::createFile(fileName2);

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / fileName, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / fileName2, EventType::FILE_CREATED));
}

// touch file
// sleep 1
// touch file2
TEST_F(DirectoryObserverTest, CreateTwoFilesWithDelay)
{
    const auto fileName = "file";
    const auto fileName2 = "file2";
    utils::test::createFile(fileName);
    utils::pauseThread(1000);
    utils::test::createFile(fileName2);

    m_timer.start();
    ASSERT_EQ(4, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / fileName, EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / fileName2, EventType::FILE_CREATED));
}

// rm F1
TEST_F(DirectoryObserverTest, DeleteFile)
{
    utils::test::deleteFile("F1");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_DELETED));
}

// rm F1 F2
TEST_F(DirectoryObserverTest, DeleteTwoFiles)
{
    utils::test::deleteFile("F1");
    utils::test::deleteFile("F2");

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_DELETED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F2", EventType::FILE_DELETED));
}

// rm F1 ; sleep 1 ; rm F2
TEST_F(DirectoryObserverTest, DeleteTwoFilesWithDelay)
{
    utils::test::deleteFile("F1");
    utils::pauseThread(1000);
    utils::test::deleteFile("F2");

    m_timer.start();
    ASSERT_EQ(4, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_DELETED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F2", EventType::FILE_DELETED));
}

// echo data >> F1
TEST_F(DirectoryObserverTest, ModifyFile)
{
    utils::test::writeDataToFile("data", "F1");

    m_timer.start();
    ASSERT_EQ(1, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED));
}

// echo data >> F1
// sleep 1
// echo data >> F1
TEST_F(DirectoryObserverTest, ModifyFileTwiceWithDelay)
{
    utils::test::writeDataToFile("data", "F1");
    utils::pauseThread(1000);
    utils::test::writeDataToFile("data", "F1", /* directoryName = */ "", /* append = */ true);

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED, 2));
}

// echo data >> F1
// echo data >> F2
TEST_F(DirectoryObserverTest, ModifyTwoFiles)
{
    utils::test::writeDataToFile("data", "F1");
    utils::test::writeDataToFile("data", "F2");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F2", EventType::FILE_MODIFIED));
}

// echo data >> F1
// sleep 1
// echo data >> F2
TEST_F(DirectoryObserverTest, ModifyTwoFilesWithDelay)
{
    utils::test::writeDataToFile("data", "F1");
    utils::pauseThread(1000);
    utils::test::writeDataToFile("data", "F2");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F2", EventType::FILE_MODIFIED));
}

// echo data >> F1
// echo data > newF1
TEST_F(DirectoryObserverTest, ModifyFileAndCreateNewFile)
{
    utils::test::writeDataToFile("data", "F1");
    utils::test::writeDataToFile("data", "newF1");

    m_timer.start();
    ASSERT_EQ(3, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "newF1", EventType::FILE_CREATED));
}

// echo data >> F1
// echo data > newF1
// sleep 1
// echo data >> F1
// echo data > newF2
TEST_F(DirectoryObserverTest, ModifyFileTwiceAndCreateTwoNewFilesWithDelay)
{
    utils::test::writeDataToFile("data", "F1");
    utils::test::writeDataToFile("data", "newF1");
    utils::pauseThread(1000);
    utils::test::writeDataToFile("data", "F1", /* directoryName = */ "", /* append = */ true);
    utils::test::writeDataToFile("data", "newF2");

    m_timer.start();
    ASSERT_EQ(6, getEventCount());
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "F1", EventType::FILE_MODIFIED, 2));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "newF1", EventType::FILE_CREATED));
    ASSERT_TRUE(eventExistsForPath(fs::canonical(PATH) / "newF2", EventType::FILE_CREATED));
}

TEST_F(DirectoryObserverTest, TryToObserveNonExistingFile)
{
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");

    m_timer.start();
    ASSERT_EQ(1, getEventCount());
    eventExistsForPath(fs::canonical(PATH) / "no_file_1", EventType::ERROR);
}

TEST_F(DirectoryObserverTest, TryToObserveNonExistingFileTwice)
{
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    eventExistsForPath(fs::canonical(PATH) / "no_file_1", EventType::ERROR, 2);
}

TEST_F(DirectoryObserverTest, TryToObserveNonExistingFileTwiceWithdelay)
{
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");
    utils::pauseThread(1000);
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    eventExistsForPath(fs::canonical(PATH) / "no_file_1", EventType::ERROR, 2);
}

TEST_F(DirectoryObserverTest, TryToObserveTwoNonExistingFiles)
{
    m_observer->startObserving(fs::canonical(PATH) / "no_file_1");
    m_observer->startObserving(fs::canonical(PATH) / "no_file_2");

    m_timer.start();
    ASSERT_EQ(2, getEventCount());
    eventExistsForPath(fs::canonical(PATH) / "no_file_1", EventType::ERROR);
    eventExistsForPath(fs::canonical(PATH) / "no_file_2", EventType::ERROR);
}

TEST_F(DirectoryObserverTest, DeleteDirectoryAndCreateFileWithSameName)
{
    utils::test::forceDeleteDirectory("D0");
    utils::test::createFile("D0");

    m_timer.start();
    ASSERT_EQ(4, getEventCount());
    eventExistsForPath(fs::canonical(PATH), EventType::FILE_MODIFIED, 2);
    eventExistsForPath(fs::canonical(PATH) / "D0", EventType::FILE_DELETED);
    eventExistsForPath(fs::canonical(PATH) / "D0", EventType::FILE_CREATED);
}

TEST_F(DirectoryObserverTest, StopObservingDirAWhenDirAAExists)
{
    stopObserving();

    fs::path root("root");

    utils::test::createDirectory(root / "a" / "b" / "c");
    utils::test::createDirectory(root / "aa" / "bb" / "cc");
    m_timer.start();

    // In SetUp fs::path(ROOT) is already observed and the count of obverved files is not zero
    // for the purpose of this test reset it to 0
    m_observer->stopObserving(fs::canonical(PATH));
    ASSERT_EQ(0, m_observer->getObservableFileCount());

    // Start observing newly created directory
    m_observer->startObserving(fs::canonical(PATH) / root);
    ASSERT_EQ(7, m_observer->getObservableFileCount());

    m_observer->stopObserving(fs::canonical(PATH) / root / "a");
    ASSERT_EQ(4, m_observer->getObservableFileCount());
}

void DirectoryObserverTest::stopObserving()
{
    if (m_observer != nullptr)
    {
        m_observer->stop();
        cleanEvents();
    }
}

void DirectoryObserverTest::TearDown()
{
    stopObserving();
    delete m_observer;

    fs::remove_all(fs::canonical(PATH));
    Test::TearDown();
}
