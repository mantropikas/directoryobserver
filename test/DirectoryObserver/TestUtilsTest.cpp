//
// Created by Mantas Antropikas on 16/05/2018.
//

#include <Utils.h>
#include "TestUtils.h"

#include "gtest/gtest.h"
#include "boost/filesystem/operations.hpp"

#include <fstream>

namespace fs = boost::filesystem;

class TestUtilsTest : public ::testing::Test
{
protected:
    void SetUp() override;

    void TearDown() override;
};

void TestUtilsTest::SetUp()
{
    Test::SetUp();
    bool createdDir = fs::create_directories(PATH + "/1/2/3");

    auto createFile = [](const std::string& filePath) -> bool
    {
        std::fstream writer;
        writer.open(filePath, std::ios::out);
        writer << CONTENT;
        writer.close();
        return fs::is_regular_file(filePath) && fs::file_size(filePath) != 0;
    };

    bool createdFile0 = createFile(PATH + "/F0");
    bool createdFile1 = createFile(PATH + "/1/F1");
    bool createdFile2 = createFile(PATH + "/1/2/F2");
    bool createdFile3 = createFile(PATH + "/1/2/3/F3");

    ASSERT_TRUE(createdDir);
    ASSERT_TRUE(createdFile0);
    ASSERT_TRUE(createdFile1);
    ASSERT_TRUE(createdFile2);
    ASSERT_TRUE(createdFile3);
}

TEST_F(TestUtilsTest, isDirectoryWithEmptyName)
{
    ASSERT_FALSE(utils::test::isDirectory(""));
}

TEST_F(TestUtilsTest, isDirectory)
{
    ASSERT_TRUE(utils::test::isDirectory("1"));
}

TEST_F(TestUtilsTest, isDirectoryRecursive)
{
    ASSERT_TRUE(utils::test::isDirectory("1/2"));
}

TEST_F(TestUtilsTest, createDirWithEmptyName)
{
    bool created = utils::test::createDirectory("");
    ASSERT_FALSE(created);
}

TEST_F(TestUtilsTest, createDir)
{
    bool created = utils::test::createDirectory("a");
    ASSERT_TRUE(created);
}

TEST_F(TestUtilsTest, createDirRecursive)
{
    bool created = utils::test::createDirectory("a/b/c");
    ASSERT_TRUE(created);
}

TEST_F(TestUtilsTest, createFileNoDirectoryWithEmptyName)
{
    bool fileCreated = utils::test::createFile("");
    ASSERT_FALSE(fileCreated);
}

TEST_F(TestUtilsTest, createFileNoDirectory)
{
    bool fileCreated = utils::test::createFile("file73");
    ASSERT_TRUE(fileCreated);
}

TEST_F(TestUtilsTest, createFileInDirectoryWithEmptyFileName)
{
    bool fileCreated = utils::test::createFile("", "1");
    ASSERT_FALSE(fileCreated);
}

TEST_F(TestUtilsTest, createFileInDirectory)
{
    bool fileCreated = utils::test::createFile("file91", "1");
    ASSERT_TRUE(fileCreated);
}

TEST_F(TestUtilsTest, deleteFileNoDirectoryWithEmptyName)
{
    bool deleted = utils::test::forceDeleteDirectory("");
    ASSERT_FALSE(deleted);
}

TEST_F(TestUtilsTest, deleteFileNoDirectory)
{
    bool deleted = utils::test::deleteFile("F0");
    ASSERT_TRUE(deleted);
}

TEST_F(TestUtilsTest, deleteFileInDirectoryWithEmptyFileName)
{
    bool deleted = utils::test::deleteFile("", "1");
    ASSERT_FALSE(deleted);
}

TEST_F(TestUtilsTest, deleteFileInDirectory)
{
    bool deleted = utils::test::deleteFile("F1", "1");
    ASSERT_TRUE(deleted);
}

TEST_F(TestUtilsTest, forceDeleteDirectoryWithEmpttyName)
{
    bool deleted = utils::test::forceDeleteDirectory("");
    ASSERT_FALSE(deleted);
}

TEST_F(TestUtilsTest, forceDeleteDirectory)
{
    bool deleted = utils::test::forceDeleteDirectory("1");
    ASSERT_TRUE(deleted);
    bool isDir = utils::test::isDirectory("1");
    ASSERT_FALSE(isDir);
}

TEST_F(TestUtilsTest, writeDataToFileNoDirectoryWithEmptyFileName)
{
    bool success = utils::test::writeDataToFile("data", "");
    ASSERT_FALSE(success);
}

TEST_F(TestUtilsTest, writeDataToFileNoDirectoryWithEmptyNameAndAppend)
{
    bool success = utils::test::writeDataToFile("data",
            /* fileName = */ "",
            /* directoryName = */ "",
            /* append = */ true);
    ASSERT_FALSE(success);
}

TEST_F(TestUtilsTest, writeDataToFileNoDirectory)
{
    auto file_sz = fs::file_size(PATH + "/F0");
    const std::string data = "data";
    const auto length = data.size();

    bool success = utils::test::writeDataToFile(data,
            /* fileName = */ "F0",
            /* directoryName = */ "");
    ASSERT_TRUE(success);

    auto file_sz_new = fs::file_size(PATH + "/F0");

    ASSERT_EQ(file_sz, CONTENT_SIZE);
    ASSERT_EQ(file_sz_new, length);
}

TEST_F(TestUtilsTest, writeDataToFileNoDirectoryAndAppend)
{
    auto file_sz = fs::file_size(PATH + "/F0");
    const std::string data = "data";
    const auto length = data.size();
    bool success = utils::test::writeDataToFile(data,
            /* fileName = */ "F0",
            /* directoryName = */ "",
            /* append = */ true);
    ASSERT_TRUE(success);

    auto file_sz_new = fs::file_size(PATH + "/F0");

    ASSERT_EQ(file_sz, CONTENT_SIZE);
    ASSERT_EQ(file_sz_new, CONTENT_SIZE + length);
}

TEST_F(TestUtilsTest, writeDataToFileInDirectoryWithEmptyName)
{
    bool success = utils::test::writeDataToFile("data",
            /* fileName = */ "",
            /* directoryName = */ "1");
    ASSERT_FALSE(success);
}

TEST_F(TestUtilsTest, writeDataToFileInDirectoryWithEmptyNameAndAppend)
{
    bool success = utils::test::writeDataToFile("data",
            /* fileName = */ "",
            /* directoryName = */ "1",
            /* append = */ true);
    ASSERT_FALSE(success);
}

TEST_F(TestUtilsTest, writeDataToFileInDirectory)
{
    auto file_sz = fs::file_size(PATH + "/1/F1");
    const std::string data = "data";
    const auto length = data.size();
    bool success = utils::test::writeDataToFile(data,
            /* fileName = */ "F1",
            /* directoryName = */ "1");
    ASSERT_TRUE(success);

    auto file_sz_new = fs::file_size(PATH + "/1/F1");

    ASSERT_EQ(file_sz, CONTENT_SIZE);
    ASSERT_EQ(file_sz_new, length);
}

TEST_F(TestUtilsTest, writeDataToFileInDirectoryAndAppend)
{
    auto file_sz = fs::file_size(PATH + "/1/F1");
    const std::string data = "data";
    const auto length = data.size();
    bool success = utils::test::writeDataToFile(data,
            /* fileName = */ "F1",
            /* directoryName = */ "1",
            /* append = */ true);
    ASSERT_TRUE(success);

    auto file_sz_new = fs::file_size(PATH + "/1/F1");

    ASSERT_EQ(file_sz, CONTENT_SIZE);
    ASSERT_EQ(file_sz_new, CONTENT_SIZE + length);
}

void TestUtilsTest::TearDown()
{
    fs::remove_all(PATH);
    Test::TearDown();
}
