//
// Created by Mantas Antropikas on 18/05/2018.
//

#include "File.h"
#include "Utils.h"
#include "TestUtils.h"

#include "gtest/gtest.h"

class FileTest : public ::testing::Test
{
};

TEST(FileTest, notExistingFile)
{
    file::File file(PATH + "/someFile");
    ASSERT_FALSE(file.exists());
    ASSERT_EQ(file.getType(), file::FileType::UNKNOWN);
    ASSERT_EQ(file.getLastWriteTime(), 0);
}

TEST(FileTest, justNowCreatedFile)
{
    const std::string data = "## FILE CONTENT ##";
    const std::string fileName = "file25";
    const std::string now = utils::getCurrentUniversalTime();
    utils::test::createDirectory(PATH);
    bool success = utils::test::createFile(fs::path(fileName));
    ASSERT_TRUE(success);

    file::File file = utils::test::getFile(fileName);

    ASSERT_TRUE(file.exists());
    ASSERT_EQ(file.getType(), file::FileType::FILE);
    ASSERT_EQ(utils::universalTimeToString(file.getLastWriteTime()), now);

    utils::test::forceDeleteDirectory(PATH);
}
