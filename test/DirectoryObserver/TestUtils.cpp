//
// Created by Mantas Antropikas on 16/05/2018.
//

#include "TestUtils.h"

#include "boost/filesystem.hpp"

#include <iostream>
#include <fstream>

namespace fs = boost::filesystem;

bool utils::test::isDirectory(const fs::path& name)
{
    fs::path directoryPath(PATH);
    directoryPath /= name;
    return !name.empty() && fs::is_directory(directoryPath);
}

bool utils::test::isFile(const fs::path& fileName, const fs::path& directoryName)
{
    if (fileName.empty())
    {
        return false;
    }

    fs::path fullPath(PATH);
    if (!directoryName.empty())
    {
        fullPath /= directoryName;
    }

    return fs::is_regular_file(fullPath / fileName);
}

bool utils::test::createDirectory(const fs::path& name)
{
    if (name.empty()) {
        return false;
    }

    fs::path directoryPath(PATH);
    directoryPath /= name;
    fs::create_directories(directoryPath);
    return fs::is_directory(directoryPath);
}

bool utils::test::createFile(const fs::path& fileName, const fs::path& directoryName)
{
    if (fileName.empty())
    {
        return false;
    }

    fs::path dirPath(PATH);
    if (!directoryName.empty())
    {
        dirPath /= directoryName;
        if (!fs::exists(dirPath) && !createDirectory(dirPath.string()))
        {
            return false;
        }
    }

    const auto filePath = dirPath / fileName;

    fs::fstream writer;
    writer.open(filePath, std::ios::out);
    writer << "data\n";
    writer.close();

    return fs::exists(filePath);
}

bool utils::test::deleteFile(const fs::path& fileName, const fs::path& directoryName)
{
    if (fileName.empty())
    {
        return false;
    }

    fs::path dirPath(PATH);
    if (!directoryName.empty())
    {
        dirPath /= directoryName;
        if (!fs::is_directory(dirPath))
        {
            return false;
        }
    }

    bool deleted = fs::exists(dirPath / fileName) && fs::remove(dirPath / fileName);
    return deleted;
}

bool utils::test::forceDeleteDirectory(const fs::path& directoryName)
{
    fs::path path(PATH);
    path /= directoryName;
    return !directoryName.empty() && fs::remove_all(path);
}

bool utils::test::writeDataToFile(const std::string& data,
                                  const fs::path& fileName,
                                  const fs::path& directoryName,
                                  bool append)
{
    if (fileName.empty())
    {
        return false;
    }

    fs::path filePath(PATH);
    if (!directoryName.empty())
    {
        filePath /= directoryName;
        if (!fs::is_directory(filePath))
        {
            return false;
        }
    }

    filePath /= fileName;

    fs::fstream writer(filePath, std::ios::out | (append ? std::ios::app : 0));
    writer << data;
    writer.close();

    return true;
}

file::File utils::test::getFile(const fs::path& fileName, const fs::path& directoryName)
{
    fs::path filePath(PATH);
    if (!directoryName.empty())
    {
        filePath /= directoryName;
    }

    filePath /= fileName;
    return file::File(filePath.string());
}
