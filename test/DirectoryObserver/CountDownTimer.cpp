//
// Created by Mantas Antropikas on 23/05/2018.
//

#include <Utils.h>
#include "CountDownTimer.h"

void CountDownTimer::start()
{
    countDown();
}

void CountDownTimer::reStart()
{
    m_ttl = 1000;
}

void CountDownTimer::countDown()
{
    while (m_ttl > 0) {
        utils::pauseThread(25);
        m_ttl -= 25;
    }

    m_ttl = 1000;
}
